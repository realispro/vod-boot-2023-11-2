package spring;

import java.time.LocalDate;
import java.util.Objects;

public class Ticket {

    private LocalDate valid;

    public Ticket(LocalDate valid) {
        Objects.requireNonNull(valid);
        this.valid = valid;
    }

    public LocalDate getValid() {
        return valid;
    }
}
