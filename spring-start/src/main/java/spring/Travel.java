package spring;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import spring.config.Air;
import spring.config.ExecutionTime;
import spring.config.TravelStartedEvent;

@Component
public class Travel {

    private final String name;
    private final Transportation transportation;
    private final Accomodation accomodation;
    private final ApplicationEventPublisher eventPublisher;

    public Travel(
            String name,
            @Air(fast = false) Transportation transportation,
            Accomodation accomodation,
            ApplicationEventPublisher eventPublisher) {
        this.transportation = transportation;
        this.accomodation = accomodation;
        this.name = name;
        this.eventPublisher = eventPublisher;
        System.out.println("constructing travel object using parametrized constructor...");
    }

    /*public Travel() {
        System.out.println("constructing travel object using default constructor...");
    }*/

    @ExecutionTime
    public void travel(Person p){
        System.out.println("started travel '" + name + "' for a person " + p);

        eventPublisher.publishEvent(new TravelStartedEvent(this));

        transportation.transport(p);
        accomodation.host(p);
        transportation.transport(p);
    }

    @Override
    public String toString() {
        return "Travel{" +
                "name='" + name + '\'' +
                ", transportation=" + transportation +
                ", accomodation=" + accomodation +
                '}';
    }
}
