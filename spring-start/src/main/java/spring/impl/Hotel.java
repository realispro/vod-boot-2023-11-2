package spring.impl;


import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import spring.Accomodation;
import spring.Person;
import spring.config.ExecutionTime;

import java.util.ArrayList;
import java.util.List;

@Component("accomodation")
public class Hotel implements Accomodation {

    @Value("#{'${hotel.meal.menu}'.split(',')}")
    private List<String> meals;

    @Value("#{'${hotel.meal.gratis:tea}'.toUpperCase()}") // SpEL
    private String gratis;

    /*@Autowired
    public void setMeals(@Qualifier("meals") List<String> meals) {
        this.meals = meals;
    }*/

    @PostConstruct
    public void afterPropertiesSet(){
        //String gratis = env.getProperty("hotel.meal.gratis");
        meals = new ArrayList<>(meals);
        meals.add(gratis);
    }

    @PreDestroy
    public void destroy(){
        System.out.println("bye bye!");
    }



    @ExecutionTime
    @Override
    public void host(Person p) {
        System.out.println("person " + p + " is being hosted in hotel. meal: " + meals);
    }


}
