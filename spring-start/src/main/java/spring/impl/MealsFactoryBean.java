package spring.impl;

import org.springframework.beans.factory.FactoryBean;

import java.util.List;

public class MealsFactoryBean implements FactoryBean<List<String>> {

    @Override
    public List<String> getObject() throws Exception {
        return List.of(
                "gazpacho",
                "spaghetti",
                "tiramisu"
        );
    }

    @Override
    public Class<?> getObjectType() {
        return List.class;
    }
}
