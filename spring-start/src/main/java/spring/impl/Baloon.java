package spring.impl;

import org.springframework.stereotype.Component;
import spring.Person;
import spring.Transportation;
import spring.config.Air;

@Component
@Air(fast = false)
public class Baloon implements Transportation {
    @Override
    public void transport(Person p) {
        System.out.println("Person '" + p + "' is travelling by baloon!");
    }
}
