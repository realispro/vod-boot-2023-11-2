package spring.config;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class FamilyNotificationListener implements ApplicationListener<TravelStartedEvent> {

    @Override
    public void onApplicationEvent(TravelStartedEvent event) {
        System.out.println("travel started event: " + event);
    }
}
