package spring.config;

import org.springframework.context.ApplicationEvent;

public class TravelStartedEvent extends ApplicationEvent {

    public TravelStartedEvent(Object source) {
        super(source);
    }
}
