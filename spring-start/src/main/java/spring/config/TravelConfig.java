package spring.config;

import org.springframework.context.annotation.*;

import java.util.List;

@Configuration
@ComponentScan("spring")
@PropertySource("classpath:/meals.properties")
@EnableAspectJAutoProxy
public class TravelConfig {

    @Bean("travelName")
    String travelName(){
        return "Summer holiday 2024";
    }

    @Bean("name")
    String name(){
        return "Winter holiday 2025";
    }


    @Bean
    List<String> meals(){
        return List.of(
             "hamburger",
             "coca-cola"
        );
    }

}
