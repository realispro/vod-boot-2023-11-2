package spring.config;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import spring.Person;
import spring.Ticket;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;

@Aspect
@Component
public class TravelAspect {

    @Pointcut("execution(public * spring.impl..*(..))")
    public void allInImpl() {
    }

    @Pointcut("execution(public * *(spring.Person))")
    public void allAcceptingPerson() {
    }

    @Before("allInImpl()")
    void logEnteringMethod(JoinPoint jp) {
        System.out.println("[ENTERING] " + jp.toLongString() + " on " + jp.getTarget().getClass().getSimpleName());
    }

    @After("allInImpl() && allAcceptingPerson()")
    void logExitingMethod(JoinPoint jp) {
        //Person person = (Person) jp.getArgs()[0];
        System.out.println("[EXITING] " + jp.toLongString() + " on " + jp.getTarget().getClass().getSimpleName());
    }

    @Around("@annotation(spring.config.ExecutionTime)")
    Object logExecutionTime(ProceedingJoinPoint jp) throws Throwable {

        Instant start = Instant.now();
        Object result = jp.proceed(jp.getArgs());
        Instant end = Instant.now();
        System.out.println("[TIME] " + jp.toString() + " execution took " + Duration.between(start, end));

        return result;
    }

    @Before("allAcceptingPerson()")
    void doAllowTrip(JoinPoint joinPoint) {
        Person person = (Person) joinPoint.getArgs()[0];
        Ticket ticket = person.getTicket();

        if (ticket == null || ticket.getValid().isBefore(LocalDate.now())) {
            throw new IllegalArgumentException("Your ticket is not valid, please buy it first!");
        }
    }
}
