package spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import spring.config.TravelConfig;

import java.time.LocalDate;

public class SpringMain {

    public static void main(String[] args) {

        Person kowalski = new Person("Jan", "Kowalski",
                new Ticket(LocalDate.now().plusDays(1)));

        ConfigurableApplicationContext context = new AnnotationConfigApplicationContext(TravelConfig.class);
                //new ClassPathXmlApplicationContext("classpath:/context.xml.bak");
        Travel travel = context.getBean(Travel.class);

        // service use
        travel.travel(kowalski);

        context.close();

        System.out.println("done.");
    }
}
