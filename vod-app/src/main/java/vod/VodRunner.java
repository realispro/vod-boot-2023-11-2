package vod;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import vod.model.Cinema;
import vod.model.Movie;
import vod.model.Rating;
import vod.service.CinemaService;
import vod.service.MovieService;

import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class VodRunner implements CommandLineRunner {

    private final CinemaService cinemaService;
    private final MovieService movieService;

    @Override
    public void run(String... args) throws Exception {
        log.info("Let's find cinemas!");
        //Class.forName("vod.repository.mem.SampleData");

        List<Cinema> cinemas = cinemaService.getAllCinemas();
        log.info(cinemas.size() + " cinemas found:");
        cinemas.forEach(System.out::println);

        Movie movie = movieService.getMovieById(2);
        List<Rating> ratings = movieService.getRatingsByMovie(movie);
        log.info("Rating of movie {}", movie.getTitle());
        ratings.forEach(rating -> log.info("rate: {}", rating.getRate()));
    }
}
