package vod.model;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
public class Rating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    @JoinColumn(name = "movie_id")
    private Movie movie;
    private float rate;

    public static Rating of(Movie movie, float value){
        Rating rating = new Rating();
        rating.setMovie(movie);
        rating.setRate(value);
        return rating;
    }

}
