package vod.config;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;
import vod.repository.CinemaDao;

import java.util.Map;

@Component
@RequiredArgsConstructor
public class VodInfoContributor implements InfoContributor {

    private final CinemaDao cinemaDao;

    @Override
    public void contribute(Info.Builder builder) {
        builder.withDetail("vod", Map.of("cinema", cinemaDao.count()));
    }
}
