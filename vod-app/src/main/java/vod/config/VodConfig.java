package vod.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import vod.repository.CinemaDao;
import vod.repository.DirectorDao;
import vod.repository.MovieDao;
import vod.repository.RatingDao;
import vod.repository.mem.MemCinemaDao;
import vod.repository.mem.MemDirectorDao;
import vod.repository.mem.MemMovieDao;
import vod.repository.mem.MemRatingDao;
import vod.service.CinemaService;
import vod.service.impl.CinemaServiceBean;
import vod.service.impl.MovieServiceBean;

import javax.sql.DataSource;

/**
 * @author KKuc1
 * @since 13.11.2023
 */

@Configuration
@ComponentScan("vod")
//@EnableTransactionManagement
public class VodConfig {


    /*@Bean("service")
    CinemaServiceBean service(CinemaDao cinemaDao, MovieDao movieDao){
        return new CinemaServiceBean(cinemaDao, movieDao);
    }*/

    /*@Bean("movieService")
    MovieServiceBean movieService(DirectorDao directorDao, CinemaDao cinemaDao, MovieDao movieDao, RatingDao ratingDao){
        return new MovieServiceBean(directorDao, cinemaDao, movieDao, ratingDao);
    }*/

    /*@Bean
    JdbcTemplate jdbcTemplate(DataSource dataSource){
        return new JdbcTemplate(dataSource);
    }*/
}