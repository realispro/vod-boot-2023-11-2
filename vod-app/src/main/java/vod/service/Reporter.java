package vod.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.*;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;
import vod.model.Director;
import vod.repository.CinemaDao;
import vod.repository.MovieDao;

import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

@Component
@RequiredArgsConstructor
@Slf4j
@EnableScheduling
@EnableAsync
public class Reporter {

    private final MovieDao movieDao;
    private final CinemaDao cinemaDao;

    @Scheduled(cron = "*/5 * * * * *")
            //(fixedRate = 10_000)
            //(fixedDelay = 3_000)
    void reportMoviesCount() throws InterruptedException {
        log.info("current movies count: {}", movieDao.count());
        Thread.sleep(10_000);
    }

    @Scheduled(cron = "*/2 * * * * *")
        //(fixedRate = 10_000)
        //(fixedDelay = 3_000)
    void reportCinemasCount() throws InterruptedException {
        log.info("current cinemas count: {}", cinemaDao.count());
    }

    @Async
    public Future<Boolean> reportMovies(Director director) {
        log.info("director {} movies: {}",
                director,
                movieDao.findByDirector(director));
        try {
            Thread.sleep(10_000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return new AsyncResult<>(new Random().nextBoolean());
    }



    //@Bean
    TaskScheduler taskScheduler(){
        ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
        threadPoolTaskScheduler.setPoolSize(5);
        threadPoolTaskScheduler.setThreadNamePrefix("vod-scheduler-");
        return threadPoolTaskScheduler;
    }


}
