package vod.repository.jdbc;

import org.springframework.jdbc.core.RowMapper;
import vod.model.Cinema;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CinemaMapper implements RowMapper<Cinema> {

    @Override
    public Cinema mapRow(ResultSet rs, int rowNum) throws SQLException {
        Cinema c = new Cinema();
        c.setId(rs.getInt("cinema_id"));
        c.setName(rs.getString("cinema_name"));
        c.setLogo(rs.getString("cinema_logo"));
        return c;
    }
}
