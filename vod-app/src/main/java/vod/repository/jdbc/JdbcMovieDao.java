package vod.repository.jdbc;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import vod.model.Cinema;
import vod.model.Director;
import vod.model.Movie;
import vod.repository.MovieDao;

import java.sql.PreparedStatement;
import java.util.List;
import java.util.Optional;


@Repository
@RequiredArgsConstructor
@ConditionalOnProperty(name="vod.dao", havingValue = "jdbc")
public abstract class JdbcMovieDao implements MovieDao {

    private static final String SELECT_ALL_MOVIES =  "select m.id as movie_id, " +
            "m.title as movie_title, m.poster as movie_poster, m.director_id as movie_director_id from movie m";

    public static final String SELECT_MOVIE_BY_ID = "select m.id as movie_id, " +
            "m.title as movie_title, m.poster as movie_poster, m.director_id as movie_director_id from movie m where id=?";

    private static final String SELECT_MOVIES_BY_CINEMA =  "select m.id as movie_id, " +
            "m.title as movie_title, m.poster as movie_poster, m.director_id as movie_director_id " +
            "from movie m inner join movie_cinema mc on mc.movie_id=m.id " +
            "where mc.cinema_id=?";

    private final JdbcTemplate jdbcTemplate;

    @Override
    public List<Movie> findAll() {
        return jdbcTemplate.query(SELECT_ALL_MOVIES, new MovieMapper());
    }

    @Override
    public Optional<Movie> findById(Integer id) {
        return Optional.ofNullable(
                jdbcTemplate.queryForObject(SELECT_MOVIE_BY_ID, new MovieMapper(), id));
    }

    @Override
    public List<Movie> findByDirector(Director d) { return null; }

    @Override
    public List<Movie> findByCinema(Cinema c) {
        return jdbcTemplate.query(SELECT_MOVIES_BY_CINEMA, new MovieMapper(), c.getId());
    }

    @Override
    public Movie save(Movie m) {

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(
                con -> {
                    PreparedStatement statement = con.prepareStatement(
                            "INSERT INTO MOVIE(TITLE,POSTER,DIRECTOR_ID) VALUES (?,?,?)",
                            new String[]{"id"});
                    statement.setString(1, m.getTitle());
                    statement.setString(2, m.getPoster());
                    statement.setInt(3, m.getDirector().getId());
                    return statement;
                },
                keyHolder
        );
        m.setId(keyHolder.getKey().intValue());

        return m;
    }


}
