package vod.repository.jpa;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vod.model.Cinema;
import vod.model.Director;
import vod.model.Movie;
import vod.repository.MovieDao;

import java.util.List;
import java.util.Optional;

@Repository
@ConditionalOnProperty(name="vod.dao", havingValue = "jpa")
public abstract class JpaMovieDao implements MovieDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Movie> findAll() {
        return entityManager
                .createQuery("select m from Movie m inner join fetch m.cinemas c inner join fetch m.director director", Movie.class)
                .getResultList();
    }

    @Override
    public Optional<Movie> findById(Integer id) {
        return Optional.ofNullable(
                entityManager.find(Movie.class, id)
        );
    }

    @Override
    public List<Movie> findByDirector(Director d) {
        return entityManager
                .createQuery("select m from Movie m inner join fetch m.cinemas c inner join fetch m.director director where director = :d", Movie.class)
                .setParameter("d", d)
                .getResultList();
    }

    @Override
    public List<Movie> findByCinema(Cinema c) {
        return entityManager
                .createQuery("select m from Movie m inner join fetch m.cinemas cinema inner join fetch m.director director where cinema = :c", Movie.class)
                .setParameter("c", c)
                .getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public Movie save(Movie m) {
        entityManager.persist(m);
        return m;
    }
}
