package vod.web.ui;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import vod.model.Cinema;
import vod.model.Movie;
import vod.service.CinemaService;
import vod.service.MovieService;

import java.util.List;

@Controller
@RequiredArgsConstructor
@Slf4j
public class MovieUIController {

    private final MovieService movieService;
    private final CinemaService cinemaService;

    @GetMapping("/movies")
    String getMovies(Model model, @RequestParam(required = false) Integer cinemaId) {
        List<Movie> movies;
        String title;

        if (cinemaId != null) {
            Cinema cinema = cinemaService.getCinemaById(cinemaId);
            movies = cinemaService.getMoviesInCinema(cinema);
            title = "Movies shown in '" + cinema.getName() + "'";
        } else {
            movies = movieService.getAllMovies();
            title = "All movies";
        }
        model.addAttribute("title", title);
        model.addAttribute("movies", movies);

        return "moviesView";
    }
}