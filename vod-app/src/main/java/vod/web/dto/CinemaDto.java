package vod.web.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import vod.web.validation.Uri;

@Data
public class CinemaDto {

    private int id;
    @NotNull
    @Size(min = 2, max = 20)
    private String name;
    @NotNull
    @Uri
    private String logo;
}
