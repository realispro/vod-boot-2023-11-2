package vod.web.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import vod.web.validation.Uri;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovieDto {

    private int id;
    @NotNull
    @Size(min = 1, max=200)
    private String title;
    @NotBlank
    @Uri
    private String poster;
    private int directorId;

}
