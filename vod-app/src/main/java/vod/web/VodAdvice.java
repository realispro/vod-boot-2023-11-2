package vod.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ResponseStatus;
import vod.web.validation.MovieValidator;

@ControllerAdvice
@RequiredArgsConstructor
@Slf4j
public class VodAdvice {

    private final MovieValidator movieValidator;

    @InitBinder("movieDto")
    void initBinder(WebDataBinder binder){
        binder.addValidators(movieValidator);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.I_AM_A_TEAPOT)
    void handleIllegalArgumentException(IllegalArgumentException e){
        log.error("illegal argument exception occurred", e);
    }

    @ExceptionHandler(Exception.class)
    ResponseEntity<String> handleException(Exception e){
        log.error("generic exception occurred", e);
        return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
    }
}
