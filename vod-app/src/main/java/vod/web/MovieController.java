package vod.web;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import vod.model.Director;
import vod.model.Movie;
import vod.service.MovieService;
import vod.service.Reporter;
import vod.web.dto.MovieDto;
import vod.web.validation.MovieValidator;

import java.net.URI;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/webapi")
public class MovieController {

    private final MovieService movieService;
    private final MessageSource messageSource;
    private final LocaleResolver localeResolver;
    private final Reporter reporter;

    @GetMapping("/movies")
    List<MovieDto> getMovies() {
        log.info("retrieve movies");
        return movieService.getAllMovies().stream().map(this::toDto).toList();
    }

    @GetMapping("/movies/{id}")
    ResponseEntity<MovieDto> getMovie(@PathVariable("id") int movieId) {
        log.info("retrieve movie by id:{}", movieId);
        Movie movie = movieService.getMovieById(movieId);
        if (movie != null) {
            return ResponseEntity.status(HttpStatus.OK).body(toDto(movie));
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @PostMapping("/movies")
    ResponseEntity<?> addMovie(@Validated @RequestBody MovieDto movieDto,
                               Errors validationErrors,
                               HttpServletRequest request) throws ExecutionException, InterruptedException {

        if(movieDto.getTitle().equals("Foo")){
            throw new IllegalArgumentException("title value Foo not allowed");
        } else if(movieDto.getTitle().equals("Bar")){
            throw new IllegalStateException("title value Bar not allowed");
        }

        Locale locale = localeResolver.resolveLocale(request);
                //new Locale("fr", "CA");
        // messages_fr_CA
        // messages_fr
        // messages_<OS>
        // messages

        //ResourceBundle resourceBundle = ResourceBundle.getBundle("messages", locale);
        //resourceBundle.getString("errors.director.missing")

        if(validationErrors.hasErrors()){
            List<String> errorMessages = validationErrors.getAllErrors().stream()
                    .map(oe->messageSource.getMessage(oe.getCode(), oe.getArguments(), locale))
                    .toList();
            return ResponseEntity.badRequest().body(errorMessages);
        }

        Movie movie = toModel(movieDto);
        Movie addedMovie = movieService.addMovie(movie);

        Future<Boolean> result = reporter.reportMovies(addedMovie.getDirector());
        log.info("report started");

        //log.info("report result: {}", result.get());

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .build(Map.of("id", movie.getId()));

        return ResponseEntity
                .created(uri)
                .body(toDto(addedMovie));
    }



    Movie toModel(MovieDto movieDto){
        Director director = movieService.getDirectorById(movieDto.getDirectorId());
        Movie movie = new Movie();
        movie.setPoster(movieDto.getPoster());
        movie.setTitle(movieDto.getTitle());
        movie.setDirector(director);
        return movie;
    }

    private MovieDto toDto(Movie movie){
        MovieDto movieDto = new MovieDto();
        movieDto.setId(movie.getId());
        movieDto.setPoster(movie.getPoster());
        movieDto.setTitle(movie.getTitle());
        movieDto.setDirectorId(movie.getDirector() != null ? movie.getDirector().getId() : 0);
        return movieDto;
    }

}
