package vod.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import vod.model.Cinema;
import vod.model.Movie;
import vod.service.CinemaService;
import vod.service.MovieService;
import vod.web.dto.CinemaDto;

import java.net.URI;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping(value = "/webapi")
public class CinemaController {

    private final CinemaService cinemaService;
    private final MovieService movieService;

    @GetMapping(value = "/cinemas", produces = "application/json")
    List<CinemaDto> getCinemas() {
        log.info("about to retrieve cinemas");
        return cinemaService.getAllCinemas().stream()
                .map(this::toDto)
                .toList();
    }

    @GetMapping("/cinemas/{cinemaId}")
    ResponseEntity<CinemaDto> getCinema(@PathVariable("cinemaId") int cinemaId) {
        log.info("about to retrieve cinema {}", cinemaId);
        Cinema cinema = cinemaService.getCinemaById(cinemaId);
        if (cinema != null) {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(toDto(cinema));
        } else {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .build();
        }
    }

    @GetMapping("/movies/{movieId}/cinemas")
    ResponseEntity<List<CinemaDto>> getCinemasShowingMovie(@PathVariable int movieId){
        log.info("about to retrieve cinemas showing movie {}", movieId);

        Movie movie = movieService.getMovieById(movieId);
        if(movie!=null) {
            List<Cinema> cinemas = cinemaService.getCinemasByMovie(movie);
            return ResponseEntity.ok(cinemas.stream().map(this::toDto).toList());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/cinemas")
    ResponseEntity<?> addCinema(@Validated @RequestBody CinemaDto cinemaDto, Errors errors){
        log.info("about to add cinema {}", cinemaDto);

        if(cinemaDto.getName().equals("Foo")){
            throw new IllegalArgumentException("Cinema name Foo not allowed");
        }

        if(errors.hasErrors()){
            return ResponseEntity.badRequest().body(errors.getAllErrors());
        }

        Cinema cinema = toModel(cinemaDto);
        cinema = cinemaService.addCinema(cinema);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .build(Map.of(
                        "id", cinema.getId()
                ));

        return ResponseEntity
                .created(uri)
                .body(toDto(cinema));
    }


    private CinemaDto toDto(Cinema cinema) {
        CinemaDto dto = new CinemaDto();
        dto.setId(cinema.getId());
        dto.setName(cinema.getName());
        dto.setLogo(cinema.getLogo());
        return dto;
    }

    private Cinema toModel(CinemaDto cinemaDto){
        Cinema cinema = new Cinema();
        cinema.setName(cinemaDto.getName());
        cinema.setLogo(cinemaDto.getLogo());
        return cinema;
    }

}
