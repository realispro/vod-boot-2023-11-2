package vod.web.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import lombok.extern.slf4j.Slf4j;

import java.net.MalformedURLException;
import java.net.URL;

@Slf4j
public class UrlValidator implements ConstraintValidator<Uri, String> {
    @Override
    public boolean isValid(String uriString, ConstraintValidatorContext constraintValidatorContext) {
        log.info("about to validate uri {}", uriString);
        try {
            new URL(uriString);
        } catch (MalformedURLException e) {
            return false;
        }
        return true;
    }
}
