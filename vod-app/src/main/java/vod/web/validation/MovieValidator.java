package vod.web.validation;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import vod.model.Director;
import vod.service.MovieService;
import vod.web.dto.MovieDto;

@Component
@RequiredArgsConstructor
public class MovieValidator implements Validator {

    private final MovieService movieService;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(MovieDto.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        MovieDto dto = (MovieDto) target;
        Director director = movieService.getDirectorById(dto.getDirectorId());
        if(director==null){
            errors.rejectValue("directorId", "errors.director.missing");
        }
    }
}
