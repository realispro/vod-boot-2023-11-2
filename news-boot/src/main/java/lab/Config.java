package lab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;
import reactor.netty.http.client.HttpClient;

@Configuration
public class Config {

    @Bean
    WebClient webClient() {
        return WebClient
                .builder()
                .clientConnector(
                        new ReactorClientHttpConnector(
                                HttpClient.create()
                                        .followRedirect(true)
                                        .secure()
                        ))
                .codecs(codecs ->
                        codecs.defaultCodecs().maxInMemorySize(1024 * 1024 * 10))
                .build();
    }

    @Bean
    Scheduler instrumentedScheduler() {
        return Schedulers.newBoundedElastic(10, 100, "Instrumented");
    }

}
