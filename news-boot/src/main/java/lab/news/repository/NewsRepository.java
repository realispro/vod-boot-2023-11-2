package lab.news.repository;

import lab.news.News;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface NewsRepository extends MongoRepository<News, Long> {
}
