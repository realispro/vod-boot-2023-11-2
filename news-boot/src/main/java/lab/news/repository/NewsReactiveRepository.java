package lab.news.repository;

import lab.news.News;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.mongodb.repository.Tailable;
import reactor.core.publisher.Flux;

public interface NewsReactiveRepository extends ReactiveMongoRepository<News, Long> {

    @Tailable
    Flux<News> getNewsBy();
}
