package lab.news.web;

import lab.news.News;
import lab.news.repository.NewsReactiveRepository;
import lab.news.repository.NewsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.util.MimeType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@RequiredArgsConstructor
@RequestMapping("/reactive")
public class NewsReactiveController {

    private final NewsReactiveRepository newsRepository;

    @GetMapping(value = "/news", produces = "text/event-stream")
    Flux<News> getNews(){
         return newsRepository.getNewsBy();
    }

}
