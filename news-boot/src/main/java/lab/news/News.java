package lab.news;


import jakarta.validation.constraints.NotNull;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data

@Document("news")
public class News {

    @Id
    private ObjectId _id;

    @NotNull
    private Category category;
    @NotNull
    private String headline;
    @NotNull
    private String text;

    public enum Category {
        POLITICS,
        SPORT,
        ENTERTAINMENT,
        SCIENCE
    }
}
