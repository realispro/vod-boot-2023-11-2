package lab.web;

import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.UUID;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
@Slf4j
class RouterConfig {

    @Bean
    RouterFunction<ServerResponse> foo() {
        return route(GET("/router/1/{id}"), this::router1)
                .andRoute(GET("/router/2/{id}"), this::router2)
                .andRoute(POST("/router"), this::readBody);
    }

    private Mono<ServerResponse> router2(ServerRequest request) {
        return ServerResponse
                .ok()
                .syncBody("Bar " + request.pathVariable("id"));
    }


    private Mono<ServerResponse> router1(ServerRequest request) {
        return ServerResponse
                .ok()
                .syncBody("Foo " + request.pathVariable("id"));
    }

    private Mono<ServerResponse> readBody(ServerRequest request) {
        return ServerResponse
                .accepted()
                .body(request
                        .bodyToMono(Person.class)
                        .map(x -> new Confirmation(UUID.randomUUID())), Confirmation.class
                );
    }

    @Value
    static class Person {
        private final String name;
        private final int age;
    }

    @Value
    static class Confirmation {
        private final UUID code;
    }

}



