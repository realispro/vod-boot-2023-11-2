package rating.command.event;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class KafkaNotifier implements RatingCommandNotifier{

    private final StreamBridge streamBridge;

    @Override
    public void emit(RatingCommandEvent event) {
        streamBridge.send("output", event);
    }
}
