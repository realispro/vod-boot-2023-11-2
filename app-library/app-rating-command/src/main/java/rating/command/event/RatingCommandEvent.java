package rating.command.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RatingCommandEvent {

    private RatingCommandType commandType;
    private long ratingId;
    private int bookId;
    private float rate;
    private String review;
}
