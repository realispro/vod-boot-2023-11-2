package rating.command.event;

public interface RatingCommandNotifier {

    void emit(RatingCommandEvent event);
}
