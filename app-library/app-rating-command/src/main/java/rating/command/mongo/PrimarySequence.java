package rating.command.mongo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("MongoSequence")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PrimarySequence {

    @Id
    private String id;
    private Long sequence;
}
