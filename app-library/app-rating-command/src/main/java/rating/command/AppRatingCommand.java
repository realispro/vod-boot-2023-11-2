package rating.command;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class AppRatingCommand {

    public static void main(String[] args) {
        SpringApplication.run(AppRatingCommand.class, args);
    }

}
