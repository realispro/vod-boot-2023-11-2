package rating.command.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity(prePostEnabled = true)
public class RatingSecurityConfig {

    @Bean
    UserDetailsService userDetailsService() {

        UserDetails user1 = User.withUsername("user1")
                .password("user1")
                .roles("ADMIN")
                .build();

        UserDetails user2 = User.withUsername("user2")
                .password("user2")
                .roles("REGULAR")
                .build();

        return new InMemoryUserDetailsManager(user1, user2);
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }

    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {

        return http

                .csrf(AbstractHttpConfigurer::disable)

                .authorizeHttpRequests(registry -> registry
                        //.requestMatchers("/ratings").hasRole("REGULAR")
                        //.requestMatchers("/ratings/**").hasRole("REGULAR")
                        .anyRequest().authenticated()
                )

                .httpBasic(Customizer.withDefaults())

                .build();

    }


}
