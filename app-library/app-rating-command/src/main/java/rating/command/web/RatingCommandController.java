package rating.command.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import rating.command.service.RatingCommandService;
import rating.command.service.RatingException;

import java.net.URI;

@RestController
@RequiredArgsConstructor
@Slf4j
public class RatingCommandController {

    private final RatingCommandService commandService;

    @Value("${rating.query.service.url}")
    private String queryServiceUrl;

    @PostMapping("/ratings")
    ResponseEntity<?> addRating(@Validated @RequestBody RatingPayload payload, Errors errors){

        log.info("about to add new rating");

        if(errors.hasErrors()){
            return ResponseEntity.badRequest().body(errors.getAllErrors());
        }

        long id = commandService.addRating(payload.getBookId(), payload.getRate(), payload.getReview());

        return ResponseEntity.created(URI.create(queryServiceUrl + "/ratings/" + id)).build();
    }

    @PutMapping("/ratings/{ratingId}")
    ResponseEntity<?> correctRating(@PathVariable int ratingId, @Validated @RequestBody RatingPayload payload, Errors errors){
        if(errors.hasErrors()){
            return ResponseEntity.badRequest().body(errors.getAllErrors());
        }

        commandService.correctRating(ratingId, payload.getRate(), payload.getReview());

        return ResponseEntity.accepted().build();
    }

    @ExceptionHandler(RatingException.class)
    ResponseEntity<String> handeRatingException(RatingException e){
        log.info("rating exception", e);
        return ResponseEntity.badRequest().body(e.getMessage());
    }
}
