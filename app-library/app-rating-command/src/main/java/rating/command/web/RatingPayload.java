package rating.command.web;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class RatingPayload {

    private int bookId;
    @Min(1)
    @Max(5)
    private float rate;
    @Size(max=256)
    private String review;

}
