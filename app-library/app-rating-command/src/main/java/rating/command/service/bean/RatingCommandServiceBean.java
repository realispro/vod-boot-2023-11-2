package rating.command.service.bean;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import rating.command.event.RatingCommandEvent;
import rating.command.event.RatingCommandNotifier;
import rating.command.event.RatingCommandType;
import rating.command.model.Rating;
import rating.command.repository.RatingRepository;
import rating.command.service.RatingCommandService;
import rating.command.service.RatingException;
import rating.command.service.book.BookClient;

@Service
@RequiredArgsConstructor
@Slf4j
public class RatingCommandServiceBean implements RatingCommandService {

    private final RatingRepository repository;
    private final BookClient bookClient;
    private final RatingCommandNotifier notifier;

    @PreAuthorize("hasRole('REGULAR')")
    @Override
    public long addRating(int bookId, float rate, String review) {

        if(bookClient.getBook(bookId)==null){
            throw new RatingException("invalid book id " + bookId);
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userName = authentication.getName();

        Rating rating = new Rating();
        rating.setRate(rate);
        rating.setBookId(bookId);
        rating.setReview(review);
        rating.setCommenter(userName);

        repository.insert(rating);

        notifier.emit(new RatingCommandEvent(
                RatingCommandType.RATING_CREATE,
                rating.getId(),
                bookId,
                rate,
                review
        ));

        return rating.getId();
    }

    @Override
    public void correctRating(int ratingId, float rate, String review) {

        Rating rating = repository.findById(ratingId)
                .orElseThrow(()->new RatingException("invalid rating id " + ratingId));

        rating.setRate(rate);
        rating.setReview(review);
        repository.save(rating);

        // TODO publish RatingCommandEvent
    }
}
