package rating.command.service.book;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "app-book", fallback = BookClientFallback.class)
public interface BookClient {

    @GetMapping("/books/{bookId}")
    BookDTO getBook(@PathVariable int bookId);

}
