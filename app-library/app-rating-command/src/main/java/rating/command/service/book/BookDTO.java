package rating.command.service.book;

import lombok.Data;

@Data
public class BookDTO {

    private int id;
    private String title;
    private String cover;
    private int authorId;
    private float price;
}
