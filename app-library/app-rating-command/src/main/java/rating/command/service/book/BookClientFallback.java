package rating.command.service.book;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class BookClientFallback implements BookClient{
    @Override
    public BookDTO getBook(int bookId) {
        log.error("fallback method for a book {}", bookId);
        return null;
    }
}
