package rating.command.service;

public interface RatingCommandService {

    long addRating(int bookId, float rate, String review);

    void correctRating(int ratingId, float rate, String review);

}
