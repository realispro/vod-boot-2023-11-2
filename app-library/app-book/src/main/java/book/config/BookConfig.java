package book.config;

import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import org.springframework.cloud.circuitbreaker.resilience4j.Resilience4JCircuitBreakerFactory;
import org.springframework.cloud.circuitbreaker.resilience4j.Resilience4JConfigBuilder;
import org.springframework.cloud.client.circuitbreaker.CircuitBreaker;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.cloud.client.circuitbreaker.Customizer;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class BookConfig {

    @Bean
    @LoadBalanced
    RestTemplate restTemplate(){
        return new RestTemplate();
    }


    @Bean
    CircuitBreaker publisherCircuitBreaker(CircuitBreakerFactory cbf){
        return cbf.create("publisher");
    }

    @Bean
    Customizer<Resilience4JCircuitBreakerFactory> globalCustomConfiguration(){

        CircuitBreakerConfig config = CircuitBreakerConfig.custom()
                .failureRateThreshold(79)

                .slidingWindow( 5, 3, CircuitBreakerConfig.SlidingWindowType.COUNT_BASED)
                //.slowCallDurationThreshold(Duration.ofMillis(10_000))
                //.slidingWindowType(CircuitBreakerConfig.SlidingWindowType.COUNT_BASED)
                //.slidingWindowSize(5)

                .build();

        return factory -> factory.configureDefault(name -> new Resilience4JConfigBuilder(name)
                .circuitBreakerConfig(config)
                .build());
    }
}
