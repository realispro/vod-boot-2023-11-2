package book.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

@FeignClient(name="app-publisher", fallback = PublisherServiceFallback.class)
public interface PublisherService {

    @GetMapping("/publishers/{publisherId}")
    PublisherDTO getPublisher(@PathVariable int publisherId);

    @GetMapping("/publishers")
    List<PublisherDTO> getPublishers(@RequestHeader("custom-header") String headerValue);

}
