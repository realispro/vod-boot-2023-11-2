package book.service.remote;

import book.service.PublisherDTO;
import book.service.PublisherService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.circuitbreaker.CircuitBreaker;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@RequiredArgsConstructor
@Slf4j
public abstract class RestPublisherService implements PublisherService {

    private final RestTemplate restTemplate;
    private final CircuitBreaker publisherCircuitBreaker;

    @Override
    public PublisherDTO getPublisher(int publisherId) {

        String publisherServiceUrl = "http://app-publisher";

        return publisherCircuitBreaker.run(
                () -> {
                    ResponseEntity<PublisherDTO> responseEntity = restTemplate
                            .exchange(
                                    publisherServiceUrl + "/publishers/" + publisherId,
                                    HttpMethod.GET,
                                    HttpEntity.EMPTY,
                                    PublisherDTO.class
                            );

                    return responseEntity.getBody();
                },
                t->{
                    log.error("fallback method", t);
                    return null;
                }
        );
    }

}
