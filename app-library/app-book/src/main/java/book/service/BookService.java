package book.service;

import book.model.Author;
import book.model.Book;
import book.repository.AuthorRepository;
import book.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class BookService {

    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;
    private final PublisherService publisherService;

    @Value("${log.prefix:[no prefix]}")
    private String logPrefix;

    public List<Book> getBooks(){
        log.info("{} about to retrieve books", logPrefix);
        return bookRepository.findAll();
    }

    public Book getBook(int id){
        log.info("{} about to retrieve book {}", logPrefix, id);
        return bookRepository.findById(id).orElse(null);
    }

    public List<Book> getBooksByAuthor(int authorId){
        log.info("{} about to retrieve books", logPrefix);
        return bookRepository.findByAuthorId(authorId);
    }

    public Book addBook(Book book, int authorId){
        log.info("{} about to add book {}", logPrefix, book);

        Optional<Author> authorOptional = authorRepository.findById(authorId);
        Author author = authorOptional.orElseThrow(()->new IllegalArgumentException(logPrefix + " invalid author id"));
        book.setAuthor(author);

        PublisherDTO publisherDTO = publisherService.getPublisher(book.getPublisherId());
        if(publisherDTO==null){
            throw new IllegalArgumentException(logPrefix + " illegal publisher id");
        }

        return bookRepository.save(book);
    }

}
