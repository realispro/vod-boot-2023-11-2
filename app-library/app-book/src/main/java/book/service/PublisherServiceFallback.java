package book.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class PublisherServiceFallback implements PublisherService{
    @Override
    public PublisherDTO getPublisher(int publisherId) {
        log.error("fallback method for id {}", publisherId);
        return null;
    }

    @Override
    public List<PublisherDTO> getPublishers(String headerValue) {
        log.error("fallback method");
        return List.of();
    }
}
