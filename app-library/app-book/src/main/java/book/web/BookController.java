package book.web;

import book.model.Book;
import book.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;

    @GetMapping("/books")
    List<BookDTO> getBooks(){
        return bookService.getBooks().stream()
                .map(BookDTO::fromData)
                .toList();
    }

    @GetMapping("/books/{bookId}")
    ResponseEntity<BookDTO> getBook(@PathVariable int bookId){
        Book book = bookService.getBook(bookId);

        return book!=null
                ? ResponseEntity.ok(BookDTO.fromData(book))
                : ResponseEntity.notFound().build();
    }

    @PostMapping("/books")
    ResponseEntity<?> addBook(
            @Validated @RequestBody BookDTO bookDTO,
            Errors errors){

        if(errors.hasErrors()){
            return ResponseEntity.badRequest()
                    .body(errors.getAllErrors());
        }

        Book book = bookService.addBook(bookDTO.toData(), bookDTO.getAuthorId());

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri()
                .path("/{bookId}")
                .build(Map.of(
                     "bookId", book.getId()
                ));

        return ResponseEntity
                .created(uri)
                .build();

    }

}
