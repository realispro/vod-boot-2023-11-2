package book.web;

import book.model.Book;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class BookDTO {

    private int id;

    @Size(min=1, message = "Title must be at least 1 character long")
    private String title;
    @NotNull(message = "book cover is required")
    private String cover;
    @Min(value = 1, message = "price must be 1PLN at least")
    private float price;
    private int publisherId;
    private int authorId;

    public static BookDTO fromData(Book data){
        BookDTO dto = new BookDTO();
        dto.setId(data.getId());
        dto.setTitle(data.getTitle());
        dto.setCover(data.getCover());
        dto.setPrice(data.getPrice());
        dto.setPublisherId(data.getPublisherId());
        dto.setAuthorId(data.getAuthor().getId());
        return dto;
    }

    public Book toData(){
        Book data = new Book();
        data.setId(this.id);
        data.setTitle(this.title);
        data.setCover(this.cover);
        data.setPrice(this.price);
        data.setPublisherId(this.publisherId);
        // TODO handle author relation
        return data;
    }
}
