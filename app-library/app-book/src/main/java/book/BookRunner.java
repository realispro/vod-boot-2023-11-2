package book;

import book.service.BookService;
import book.service.PublisherService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class BookRunner implements CommandLineRunner {

    private final BookService bookService;
    private final PublisherService publisherService;

    @Override
    public void run(String... args) throws Exception {
        bookService.getBooksByAuthor(1)
                .forEach(book->log.info("{}", book));
        publisherService.getPublishers("from-runner")
                .forEach(publisher->log.info("publisher: {}", publisher));
    }
}
