package book.repository;


import book.model.Author;
import book.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface BookRepository extends JpaRepository<Book, Integer> {

    //List<Book> findAll();

    //Optional<Book> findById(int id);

    List<Book> findByAuthorId(int authorId);

    //Book save(Book p);
}
