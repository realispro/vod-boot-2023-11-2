package publisher.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import publisher.model.Publisher;

public interface PublisherRepository extends JpaRepository<Publisher, Integer> {
}
