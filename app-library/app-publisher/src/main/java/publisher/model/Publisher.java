package publisher.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Data;

@Data
@Entity
public class Publisher {

    @Id
    @GeneratedValue
    private Integer id;
    private String name;
    private String logo;
}
