package publisher.web;

import lombok.Data;
import publisher.model.Publisher;

@Data
public class PublisherDTO {

    private int id;
    private String name;
    private String logo;

    public static PublisherDTO fromData(Publisher data){
        PublisherDTO dto = new PublisherDTO();
        dto.setId(data.getId());
        dto.setName(data.getName());
        dto.setLogo(data.getLogo());
        return dto;
    }

}
