package first;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Lazy(value = false)
@Slf4j
public class GreetingsBean {

    @Value("${greetings.name:Joe}")
    private String name;


    @PostConstruct
    void postConstruct(){
        log.info("greetings bean created");
    }


    public String sayHello(){
        return "Hey " + name + "!";
    }
}
