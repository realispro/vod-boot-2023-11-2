db = db.getSiblingDB('news');

db.createUser({
    user: 'admin',
    pwd: 'admin',
    roles: [
        {
            role: 'readWrite',
            db: 'admin',
        },
    ],
});

db.createCollection('news', {capped: true, size: 100000});

db.news.insertMany([
    {
        headline: 'Bill Palmer',
        text: 'abc',
        category: 'SPORT'
    },
    {
        headline: 'Lost',
        text: 'again',
        category: 'POLITICS'
    }
]);

db.news.insertMany([
    {
        headline: 'UFO visited Pentagon!',
        text: 'And requested X-files',
        category: 'POLITICS'
    }
]);
